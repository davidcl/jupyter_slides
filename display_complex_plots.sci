// this is an helper script for displaying a complex plot in "Scilab jupyter introduction"

function display_complex_plots()

    scf(1)
    
    subplot(1,2,2)
    Matplot1()
    xtitle("Matplot1() example","","")
    
    subplot(2,2,1)
    histplot
    delete(findobj("type","Legend"))
    
    subplot(2,2,3)
    title("Smaller plots:", "fontsize",3)
    
    subplot(2,4,5)
    polarplot()
    
    subplot(2,4,6)
    param3d()
    xtitle("","","","")
    
    gcf().axes_size = [670 400];

endfunction